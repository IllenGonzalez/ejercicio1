package edu.unimagdalena.logica;

public class Alumno {
	private long id;
	private String nombre;
	private double promedio;		
	
	public Alumno(long id, String nombre, double promedio) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.promedio = promedio;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public double getPromedio() {
		return promedio;
	}

	public void setPromedio(double promedio) {
		this.promedio = promedio;
	}

	public void imprimir(){
		System.out.println("--------------");
		System.out.println("ID: " + id);
		System.out.println("Nombre: " + nombre);
		System.out.println("Promedio: " + promedio);		
		
	}	

}
