package edu.unimagdalena.logica;

import java.util.ArrayList;

public class Curso {
	private String nombre;
	private ArrayList<Alumno> alumnos;
	public Curso(String nombre){
		this.nombre = nombre;
		alumnos = new ArrayList<Alumno>();
	}
	public void matricular(Alumno a){
		if(estaInscrito(a)){
			System.out.println(a.getNombre() + ("ya esta inscrito"));
		}else{
			alumnos.add(a);
		}
		
	}
	public boolean remover(Alumno a){
		boolean r = alumnos.remove(a);
		return r;
		//return alumnos.remove(a);
	}
	public boolean estaInscrito(Alumno a){
		return alumnos.contains(a);
	}
	public void imprimirAlumnos(){
		for(Alumno al: alumnos){
			al.imprimir();
		}
	}

}
