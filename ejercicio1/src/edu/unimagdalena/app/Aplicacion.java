package edu.unimagdalena.app;

import edu.unimagdalena.logica.Alumno;
import edu.unimagdalena.logica.Curso;

public class Aplicacion {
	public static void main(String[] arg){
		Alumno a = new Alumno(1, "ALFA", 6.5);
		Alumno b = new Alumno(2, "BETA", 4.5);
		Alumno g = new Alumno(3, "GAMA", 5.5);
		
		
		Curso c = new Curso("PEF2501");
		c.imprimirAlumnos();
		
		
		c.matricular(a);
		c.matricular(b);
		
		if (c.estaInscrito(b)){
			System.out.println(b.getNombre() + "Se encuentra inscrito");
		}else{
			System.out.println(b.getNombre() + "No esta inscrito");
		}
		if (c.estaInscrito(b)){
			System.out.println(g.getNombre() + "Se encuentra inscrito");
		}else{
			System.out.println(g.getNombre() + "No esta inscrito");
		}
		
		c.remover(b);
		c.imprimirAlumnos();
		
		
	}
	

}
